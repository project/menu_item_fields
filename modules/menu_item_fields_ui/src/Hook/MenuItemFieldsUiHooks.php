<?php

namespace Drupal\menu_item_fields_ui\Hook;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * UI related hooks.
 */
class MenuItemFieldsUiHooks {

  use StringTranslationTrait;

  /**
   * Implements hook_entity_type_alter().
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   An associative array of all entity type definitions.
   */
  #[Hook('entity_type_alter')]
  public function entityTypeAlter(array &$entity_types): void {
    if (isset($entity_types['menu_link_content'])) {
      $entity_types['menu_link_content']->set('field_ui_base_route', 'entity.menu.collection');
    }
  }

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  #[Hook('form_block_form_alter')]
  public function formAlter(&$form, FormStateInterface $form_state): void {
    $build_info = $form_state->getBuildInfo();
    $plugin_id = $build_info['callback_object']->getEntity()->getPluginId();
    if (!str_starts_with($plugin_id, 'menu_item_fields:')) {
      return;
    }

    $form['settings']['view_mode']['#description'] = $this->t('View mode to use when rendering menu items. <a href="@url">Configure view modes</a>', [
      '@url' => Url::fromRoute('entity.entity_view_display.menu_link_content.default')->toString(),
    ]);
  }

}
