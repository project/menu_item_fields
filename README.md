# Description

The main purpose of the module is to be able to add fields to
custom menu items and render them with different view modes.

## Installation and usage

First enable the `menu_item_fields_ui` module since is
needed to create the fields and view modes.

In order to render a menu with the configured fields
a new block is provided with the module called "Menu with fields".

After the menu is built the UI module can be disabled
in production just like the `field_ui` module.

## Features

* Add fields to menu items and sort their view and form displays.
* Have different form modes for each menu. Create and enable
a form mode with the same machine name as the menu.
* Show menu items with a block choosing the view mode.
* Configure additional options on the formatter like
the `rel` and `target` attributes.
* Add more attributes with the [Link attributes](https://www.drupal.org/project/link_attributes/) module.
* Optionally have a field on the menu link entity that
overrides the display mode for each item.
  * Is up to the site builder to create it.
  * This field needs to store the string value of the
  display mode, for example: 'mega'.
  * The field that stores the view mode is configured on the block.

## Release 2.x

8.x-1.x releases of the module implemented a separate theme definition
`menu__field_content` and used it only when the block provided by this
module was rendered. The double underscore was a weak design decision
that was handled by Drupal core in strange ways, sometimes as its own theme
hook and sometimes as a suggestion of the `menu` hook.

In the 2.0 release the additional hook definition is eliminated in favor of
altering the `menu` theme hook. The template path from the `system` module is overridden to be
able to render the menu entities.

## Migrate from 8.x-1.x to 2.x

If your theme implemented `menu.html.twig` you need to adapt it by looking
at the `menu.html.twig` from this module to be able to render additional fields different than
the link.

If in the 8.x-1.x series your theme had a menu--field-content.html.twig rename it to just
`menu.html.twig`.

Theme that don't override `menu.html.twig` will pick this module implementation.

Olivero is an example of a a theme that doesn't work out of the box.

## Similar modules

* [Menu item extras](https://www.drupal.org/project/menu_item_extras):
* Provides a bundle for each menu while this module does not add any new bundle.
* With Menu item extras the children output can be sorted on the
"Manage display" interface. On Menu Item Content Fields the
children are below the parent since the template tries to be as
close as possible to Drupal core.
* In general this module tries to be more simple trying to
override as few templates as possible.

## Future improvements

Being able to load the field information into other kind of menu items so
all the menu items can be rendered similarly (e.g with icons)
and not only custom menu items.

For incorporating fields on menu items on Drupal core
there is [a proposal to make it so](https://www.drupal.org/project/ideas/issues/3047131)
since is just enabling an interface.

## Notes

Inspired by the [Menu Link Content fields](https://www.drupal.org/project/menu_link_content_fields) module
and of course Menu Link Extras.
