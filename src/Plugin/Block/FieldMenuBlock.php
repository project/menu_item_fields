<?php

namespace Drupal\menu_item_fields\Plugin\Block;

use Drupal\Core\Block\Attribute\Block;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\system\Form\SystemMenuOffCanvasForm;
use Drupal\system\Plugin\Block\SystemMenuBlock;
use Drupal\system\Plugin\Derivative\SystemMenuBlock as SystemMenuBlockDeriver;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a drupal menu that uses display view modes.
 */
#[Block(
  id: "menu_item_fields",
  admin_label: new TranslatableMarkup("Menu with fields"),
  category: new TranslatableMarkup("Menu item Fields"),
  deriver: SystemMenuBlockDeriver::class,
  forms: [
    'settings_tray' => SystemMenuOffCanvasForm::class,
  ]
)]
class FieldMenuBlock extends SystemMenuBlock {

  /**
   * Constructs a new FieldMenuBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menu_tree
   *   The menu tree service.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The active menu trail service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MenuLinkTreeInterface $menu_tree,
    MenuActiveTrailInterface $menu_active_trail,
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $menu_tree, $menu_active_trail);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    // @phpstan-ignore-next-line
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree'),
      $container->get('menu.active_trail'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $defaultConfiguration = parent::defaultConfiguration() + [
      'view_mode' => 'default',
      'view_mode_override_field' => '_none',
    ];
    return $defaultConfiguration;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $viewModes = $this->entityTypeManager->getStorage('entity_view_mode')->loadByProperties([
      'targetEntityType' => 'menu_link_content',
    ]);
    $viewModeOptions = ['default' => $this->t('Default')];
    foreach ($viewModes as $viewMode) {
      $id = substr($viewMode->id(), strlen('menu_link_content.'));
      $viewModeOptions[$id] = $viewMode->label();
    }

    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode to use when rendering the menu items'),
      '#default_value' => $this->configuration['view_mode'],
      '#options' => $viewModeOptions,
    ];

    $fields = $this->entityTypeManager->getStorage('field_config')->loadByProperties([
      'entity_type' => 'menu_link_content',
    ]);
    $fieldOptions = ['_none' => $this->t('None')];
    foreach ($fields as $field) {
      [,, $id] = explode('.', $field->id());
      $fieldOptions[$id] = $field->label();
    }

    $form['view_mode_override_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field that stores the view mode override per menu item'),
      '#default_value' => $this->configuration['view_mode_override_field'],
      '#description' => $this->t('This field will usually be a an options field with the available view mode ids.'),
      '#options' => $fieldOptions,
    ];
    $form += parent::blockForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    parent::blockSubmit($form, $form_state);
    $this->configuration['view_mode'] = $form_state->getValue('view_mode');
    $this->configuration['view_mode_override_field'] = $form_state->getValue('view_mode_override_field');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = parent::build();

    $build['#view_mode'] = $this->configuration['view_mode'];
    if ($this->configuration['view_mode_override_field'] != '_none') {
      $build['#view_mode_override_field'] = $this->configuration['view_mode_override_field'];
    }

    return $build;
  }

}
