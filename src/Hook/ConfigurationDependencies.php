<?php

namespace Drupal\menu_item_fields\Hook;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityViewModeInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\field\FieldConfigInterface;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Hooks to handle configuration dependencies.
 */
class ConfigurationDependencies {

  /**
   * Implements hook_entity_view_display_presave().
  */
  #[Hook('entity_view_display_presave')]
  public function entityViewDisplayPresave(EntityViewDisplayInterface $entity): void {
    if ($entity->get('targetEntityType') === 'menu_link_content') {
      $this->setConfigDependencies($entity);
    }
  }

  /**
   * Implements hook_entity_form_display_presave().
   */
  #[Hook('entity_form_display_presave')]
  public function entityFormDisplayPresave(EntityFormDisplayInterface $entity): void {
    if ($entity->get('targetEntityType') === 'menu_link_content') {
      $this->setConfigDependencies($entity);
    }
  }

  /**
   * Implements hook_entity_view_mode_presave().
   */
  #[Hook('entity_view_mode_presave')]
  public function entityViewModePresave(EntityViewModeInterface $entity): void {
    if ($entity->get('targetEntityType') === 'menu_link_content') {
      $this->setConfigDependencies($entity);
    }
  }

  /**
   * Implements hook_field_config_presave().
   */
  #[Hook('field_config_presave')]
  public function fieldConfigPresave(FieldConfigInterface $entity): void {
    if ($entity->get('entity_type') === 'menu_link_content') {
      $this->setConfigDependencies($entity);
    }
  }

  /**
   * Implements hook_field_storage_config_presave().
   */
  #[Hook('field_storage_config_presave')]
  public function fieldStorageConfigPresave(FieldStorageConfigInterface $entity): void {
    if ($entity->get('entity_type') === 'menu_link_content') {
      $this->setConfigDependencies($entity);
    }
  }

  /**
   * Set proper dependencies on the config entities created with this module.
   */
  protected function setConfigDependencies(ConfigEntityInterface $entity): void {
    $dependencies = $entity->get('dependencies');
    if (array_search('menu_item_fields', $dependencies['module'] ?? []) === FALSE) {
      $dependencies['module'][] = 'menu_item_fields';
    }
    $entity->set('dependencies', $dependencies);
  }

}
