<?php

namespace Drupal\menu_item_fields\Hook;

use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Hook\Attribute\Hook;

/**
 * Add contextual link to edit the menu.
 */
class ContextualLink {

  /**
   * Build a Contextual link handler.
   */
  public function __construct(protected ModuleHandlerInterface $moduleHandler) {
  }

  /**
   * Implements hook_block_view_BASE_BLOCK_ID_alter() for 'menu_item_fields'.
   *
   * @param array $build
   *   A renderable array of data.
   * @param \Drupal\Core\Block\BlockPluginInterface $block
   *   The block plugin instance.
   */
  #[Hook('block_view_menu_item_fields_alter')]
  public function blockViewSystemMenuBlockAlter(array &$build, BlockPluginInterface $block): void {
    if ($block->getBaseId() === 'menu_item_fields' && $this->moduleHandler->moduleExists('menu_ui')) {
      $menuName = $block->getDerivativeId();
      $build['#contextual_links']['menu'] = ['route_parameters' => ['menu' => $menuName]];
    }
  }

}
