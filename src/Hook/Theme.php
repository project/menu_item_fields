<?php

namespace Drupal\menu_item_fields\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Extension\ModuleExtensionList;

/**
 * Theme related hooks.
 */
class Theme {

  /**
   * Build a theme hook handler.
   */
  public function __construct(protected ModuleExtensionList $moduleList) {
  }

  /**
   * Implements hook_theme().
   *
   * @return array
   *   Theme definitions.
   */
  #[Hook('theme')]
  public function theme(): array {
    return [
      // Hook for the entity template.
      'menu_link_content' => [
        'render element' => 'content',
      ],
    ];
  }

  /**
   * Implements hook_theme_registry_alter().
   *
   * @param array $themeRegistry
   *   The entire cache of theme registry information, post-processing.
   */
  #[Hook('theme_registry_alter')]
  public function themeRegistryAlter(&$themeRegistry): void {
    if (!isset($themeRegistry['menu'])) {
      return;
    }

    // Override the template from the system module
    // to use the one provided by this module.
    if ($themeRegistry['menu']['path'] === 'core/modules/system/templates') {
      $templatesPath = $this->moduleList->getPath('menu_item_fields') . '/templates';
      $themeRegistry['menu']['path'] = $templatesPath;
    }

    // Add additional variables to allow swapping view modes.
    $themeRegistry['menu']['variables']['view_mode'] = 'default';
    $themeRegistry['menu']['variables']['view_mode_override_field'] = '';
  }

}
